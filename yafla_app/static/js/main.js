// YAFLA JS functions

// Check if user exists or not
$("#signup-user").change(function(){
    var username = $("#signup-user").val();
    console.log(username);
    if (username == ""){
        if ($(".has-feedback").hasClass("has-error")){
            $(".has-feedback").removeClass("has-error");
        };
        if (!$(".glyphicon-remove").hasClass("hidden")){
            $(".glyphicon-remove").addClass("hidden");
        };

        if ($(".has-feedback").hasClass("has-success")){
            $(".has-feedback").removeClass("has-success");
        };
        if (!$(".glyphicon-ok").hasClass("hidden")){
            $(".glyphicon-ok").addClass("hidden");
        };
        return false;
    };
    var url = window.location.protocol + '//' + window.location.host;
    var uri = '/signup';
    $.ajax({
        method: "GET",
        url: url + uri,
        data: {
            username: username
        },
        statusCode:{
            404: function(){
                if ($(".has-feedback").hasClass("has-error")){
                    $(".has-feedback").removeClass("has-error");
                };
                if (!$(".glyphicon-remove").hasClass("hidden")){
                    $(".glyphicon-remove").addClass("hidden");
                };
                $(".has-feedback").addClass("has-success");
                $(".glyphicon-ok").removeClass("hidden");
                $("#signup").removeClass("disabled");
            },
            200: function(){
                if ($(".has-feedback").hasClass("has-success")){
                    $(".has-feedback").removeClass("has-success");
                };
                if (!$(".glyphicon-ok").hasClass("hidden")){
                    $(".glyphicon-ok").addClass("hidden");
                };
                $(".has-feedback").addClass("has-error");
                $(".glyphicon-remove").removeClass("hidden");
                $("#signup").addClass("disabled");
            }
        }
    });
});

// User Signup
$("#signup").click(function(){
    var username = $("[name='username']").val();
    var password = $("[name='password']").val();
    if (username == "" || password == ""){
        $(".message").addClass("text-danger");
        $(".message").text("Username or Password cannot be empty! ");
        setTimeout(function() { $(".message").hide(); }, 2000);
        return false;
    }
    var data =  JSON.stringify({username: username,
                                password: password});
    var url = window.location.protocol + '//' + window.location.host;
    var uri = '/signup';

    $.ajax({
        type: "POST",
        url: url + uri,
        data: data,
        dataType: 'json',
        contentType:"application/json; charset=utf-8",
        statusCode:{
            200: function(){
                $(".message").addClass("text-success");
                $(".message").text("Sign Up successful. You may now login");
                setTimeout(function() { $(".message").hide(); }, 5000);
            }
        }
    });
});


// User Signin
$("#signin").click(function(){
    var username = $("[name='signin-user']").val();
    var password = $("[name='signin-password']").val();
    if (username == "" || password == ""){
        return false;
    }

    var client_id = '3a4dc674-3e68-11e5-b906-525400a4585b';
    var url = window.location.protocol + '//' + window.location.host;
    var uri = '/login';
    var data =  JSON.stringify(
        {
            username: username,
            password: password,
            client_id: client_id
        });
    $.ajax({
        type: "POST",
        url: url + uri,
        data: data,
        dataType: 'json',
        contentType:"application/json; charset=utf-8",
        statusCode:{
            200: function(response){
                Cookies.set('access_token', response.access_token,
                            {expires: response.expires} );
                Cookies.set('refresh_token', response.refresh_token);
                $.session.set('logged_in', 'True');
                location.href = url + '/userfeed';
            }
        }
    });
});
