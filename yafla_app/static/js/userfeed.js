// YAFLA JS functions

// Logout
$("#logout").click(function(){
    var username = $.session.get('username');
    var url = window.location.protocol + '//' + window.location.host;
    var uri = '/logout';
    var data = JSON.stringify({username: username});
    $.ajax({
        type: "POST",
        url: url + uri,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        statusCode:{
            200: function(response){
                Cookies.remove('access_token');
                Cookies.remove('refresh_token');
                $.session.remove('logged_in');
                location.href = url;
            }
        }
    });
});


// Image delete
$(".glyphicon-trash").click(function(){
    var username = $.session.get('username');
    var id = $(this).attr('id');
    var image_id = id.split("_").pop();
    var url = $("#image_"+image_id).attr('src');
    var delete_url = url.replace(".jpg", "");
    $.ajax({
        type: "DELETE",
        url: delete_url,
        statusCode:{
            200: function(response){
                location.href = window.location.protocol + '//' + window.location.host + '/userfeed';
            }
        }
    });
});

// Image upload
$("#upload").click(function(){
    var formData = new FormData($("#form")[0]);
    var url = window.location.protocol + '//' + window.location.host;
    var uri = '/upload';
    $.ajax({
        url: url + uri,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        statusCode:{
            200: function(response){
                location.href = url + '/userfeed';
            },
            403: function(response){
                $('.message').addClass('text-warning');
                console.log(response.message);
                $('.message').text("Warning: Duplicate image!");
                setTimeout(function() { $(".message").hide(); }, 3000);
            }
        }
    });
});
