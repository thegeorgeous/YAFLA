from flask import Blueprint, request, render_template, redirect, url_for, session
from yafla_app.models.photos import Photo, Tag
from yafla_app.models.users import Users
from yafla_app.utils import user_exists

home_bp = Blueprint('home', __name__,
                    template_folder='templates')


@home_bp.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        if request.cookies.get('access_token'):
            return redirect(url_for('.userfeed'))
        return render_template('index.html')


@home_bp.route('/userfeed', methods=['GET', 'POST'])
def userfeed():
    if request.method == 'GET':
        if request.cookies.get('access_token'):
            host = request.headers.get('Host')
            username = session.get('username')
            user = user_exists(username)
            if user:
                photos = Photo.query.filter_by(user_id=user.id).order_by((Photo.id.desc())).all()
                data = []
                for photo in photos:
                    image = {}
                    image['url'] = "http://" + host + "/" + username + "/images/" + photo.image_name
                    tags = Tag.query.filter_by(image_id=photo.id).all()
                    if tags:
                        image['tags'] = []
                        for tag in tags:
                            user = Users.query.get(tag.user_id)
                            image['tags'].append(user.username)
                    data.append(image)
                return render_template('userfeed.html', data=data)
        return redirect(url_for('.home'))
