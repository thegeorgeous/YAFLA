# python imports
import os
import uuid

# flask imports
from flask import Blueprint, jsonify, request, url_for, send_from_directory, session
from werkzeug import secure_filename
from PIL import Image

# application imports
from yafla_app import db, ALLOWED_EXTENSIONS, UPLOAD_FOLDER
from yafla_app.utils import user_exists, dhash
from yafla_app.models.photos import Photo, Tag
from yafla_app.models.users import Users

photo_bp = Blueprint('photo', __name__,
                     template_folder='templates')


@photo_bp.route('/upload', methods=['POST'])
def photo_upload():
    """ This view handles the photo upload service
    Request is multipart form data. Response is an image url
    """
    if request.method == 'POST':
        username = session.get('username')
        user = user_exists(username)

        if user:
            user_id = user.id
        file = request.files['file']

        if file and allowed_file(file.filename):
            im = Image.open(file)
            image_hash = dhash(im)
            old_hash = Photo.query.filter_by(image_hash=image_hash).first()
            if not old_hash:
                upload_folder = UPLOAD_FOLDER + "/" + username
                if not os.path.exists(upload_folder):
                    os.makedirs(upload_folder)
                filename = str(uuid.uuid1()).replace('-', '') + '.' + \
                    secure_filename(file.filename).rsplit('.', 1)[1]
                im.save(os.path.join(upload_folder, filename), format='JPEG')

                photo = Photo(image_name=filename)
                photo.user_id = user_id
                photo.image_hash = image_hash
                db.session.add(photo)
                db.session.commit()

                tagged_users = request.form.get('tags')
                if tagged_users:
                    tagged_users = tagged_users.split(',')
                    for tag_user in tagged_users:
                        user = Users.query.filter_by(username=tag_user.strip(" ")).first()
                        if user:
                            tag = Tag(user_id=user.id, image_id=photo.id)
                            db.session.add(tag)
                            db.session.commit()

                return jsonify({"uri": url_for('.uploaded_file', username=username,
                                               filename=filename, _external=True)}), 200
            return jsonify({"message": "Warning! Duplicate Image"}), 403
    return jsonify({"message": "Bad Request"}), 401


@photo_bp.route('/<username>/images/<filename>', methods=['GET'])
def uploaded_file(filename, username):
    upload_folder = UPLOAD_FOLDER + '/' + username
    return send_from_directory(upload_folder,
                               filename)


@photo_bp.route('/<username>/images/<filename>', methods=['DELETE'])
def delete_image(username, filename):
    file_location = UPLOAD_FOLDER + '/' + username + '/'
    filename = filename + '.jpg'
    if os.path.isfile(file_location + filename):
        os.remove(file_location + filename)
        photo = Photo.query.filter_by(image_name=filename).first()
        tags = Tag.query.filter_by(image_id=photo.id).all()
        if tags:
            for tag in tags:
                db.session.delete(tag)
                db.session.commit()

        db.session.delete(photo)
        db.session.commit()
        return jsonify({"message": "Success"}), 200
    return jsonify({"message": "Not Found"}), 404


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
