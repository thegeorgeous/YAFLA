import datetime
from flask import Blueprint, request, jsonify, session

from yafla_app import db
from yafla_app.models.users import Users, OAuth
from yafla_app.utils import user_exists
from oauthlib.common import generate_token

CHARACTER_SET = ('abcdefghijklmnopqrstuvwxyz'
                 '0123456789')

CLIENT_ID = '3a4dc674-3e68-11e5-b906-525400a4585b'

account_bp = Blueprint('account', __name__,
                       template_folder='templates')


@account_bp.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        username = request.args.get('username')
        if user_exists(username):
            return jsonify({"message": "Username exists"}), 200
        return jsonify({"message": "Username not found"}), 404

    elif request.method == 'POST':
        username = request.json.get('username')
        password = request.json.get('password')

        if not user_exists(username):
            new_user = Users(username)
            new_user.password = password
            db.session.add(new_user)
            db.session.commit()
            return jsonify({
                "message": "Your account has been created, You may now Login"}), 200
        return jsonify({"message": "Account already exists"}), 403


@account_bp.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        username = request.json.get('username')
        password = request.json.get('password')

        user = Users.query.filter_by(username=username).first()
        client_id = request.json.get('client_id')
        if client_id == CLIENT_ID and user.password == password:
            token = generate_token(length=40, chars=CHARACTER_SET)
            refresh_token = generate_token(length=40,
                                           chars=CHARACTER_SET)
            expires = datetime.datetime.now() +\
                datetime.timedelta(seconds=3600)
            scope = 'read_write'
            user_id = user.id

            purge_tokens(user.id)

            oauth = OAuth(user_id=user_id, token=token,
                          refresh_token=refresh_token,
                          expires=expires, scope=scope)

            db.session.add(oauth)
            db.session.commit()
            session['username'] = user.username
            response = {
                "access_token": token,
                "token_type": "Bearer",
                "expires_in": 3600,
                "refresh_token": refresh_token,
                "redirect_uri": "/home",
                "username": user.username
            }

            return jsonify(response), 200
        return jsonify({"message": "Bad Request"}), 400


@account_bp.route('/logout', methods=['POST'])
def logout():
    if request.method == 'POST':
        username = session.get('username')
        user = Users.query.filter_by(username=username).first()
        purge_tokens(user.id)
        session.pop('username', None)
        return jsonify({"message": "Logged out"}), 200


def purge_tokens(user_id):
    old_auth = OAuth.query.filter_by(user_id=user_id).all()
    for old_token in old_auth:
        db.session.delete(old_token)
        db.session.commit()
