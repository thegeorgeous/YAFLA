from yafla_app import db


class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    image_name = db.Column(db.String(80))
    image_hash = db.Column(db.String(80))

    def __init__(self, image_name):
        self.image_name = image_name

    def __repr__(self):
        return '<Photo %r>' % self.image_name


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    image_id = db.Column(db.Integer, db.ForeignKey('photo.id'))

    def __init__(self, image_id, user_id):
        self.image_id = image_id
        self.user_id = user_id

    def __repr__(self):
        return '<Tag %r>' % self.user_id
