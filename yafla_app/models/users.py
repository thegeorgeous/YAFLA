from yafla_app import db


class Users(db.Model):
    __table_name__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(20))
    images = db.relationship('Photo', backref='user',
                             lazy='dynamic')

    def __init__(self, username):
        self.username = username

    def __repr__(self):
        return '<User %r>' % self.username


class OAuth(db.Model):
    __table_name__ = 'oauth'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship("Users", backref='user',
                           lazy='joined')
    token = db.Column(db.String(40), unique=True)
    expires = db.Column(db.DateTime)
    refresh_token = db.Column(db.String(40), unique=True)
    scope = db.Column(db.String(10))

    def __init__(self, **kargs):
        self.user_id = kargs.get('user_id')
        self.token = kargs.get('token')
        self.expires = kargs.get('expires')
        self.refresh_token = kargs.get('refresh_token')
        self.scope = kargs.get('scope')

    def __repr__(self):
        return '<Token %r>' % self.token
